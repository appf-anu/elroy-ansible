Clone repo,

Create .vault_password file with the correct vault password (see https://gitlab.com/appf-anu/docs/-/wikis/Elroy-Ansible-Configuration)

make sure you are connected to wireguard vpn:

```bash
ansible-playook main.yml # just do everything

ansible-playbook --tags=<some-tags> main.yml # only run specific tags
```

available tags:

* **telegraf, telegraf-main**: main telegraf
* **ups**: reporting for the ups, usually the usb connection to the ups is broken
* **mqtt**: mqtt server and telegraf
* **conviron2, conviron**: conviron chambers
* **conviron2-monitor**: conviron chambers on monitor with http
* **conviron2-run**: conviron chambers being run by telnet
* **lights**: all the lights
* **heliospectra2**: all the heliospectra lights
* **heliospectra2-mon**: heliospectra lights to be monitored
* **heliospectra2-run**: heliospectra lights being controlled from files
* **fytopanel**: all the psi lights
* **fytopanel-run**: psi lights to be controlled via usb serial inteface.