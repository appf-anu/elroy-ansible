#!/bin/python3
import datetime
import random
import sys
import os
from openpyxl import load_workbook


def round_seconds(datetime_obj):
    new_datetime_obj = datetime_obj + datetime.timedelta(seconds=0.5)
    return new_datetime_obj.replace(microsecond=0)


fn = sys.argv[1]
wb = load_workbook(fn)
ws = wb['timepoints']
first_date_cell = ws.cell(column=1, row=2)
start_date = round_seconds(first_date_cell.value)
assert type(start_date) is datetime.datetime

td = datetime.date.today()
today = datetime.datetime.combine(td, datetime.datetime.min.time())

diff = today - start_date


for row in ws.iter_rows():
    if row[0].value == "datetime":
        continue
    try:
        olddt = round_seconds(row[0].value)
        newdt = (olddt + diff).replace(hour=olddt.hour, minute=olddt.minute, second=olddt.second)
        print(f'{olddt}\t->\t{newdt}')
        row[0].value = newdt
    except Exception as e:
        print(str(e))


fn, ext = os.path.splitext(fn)

wb.save(filename=f"{fn}_adjustedto{datetime.date.today().isoformat()}{ext}")
