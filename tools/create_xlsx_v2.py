#!/bin/python3
import datetime
import random
import shutil
import os
import math
from openpyxl import Workbook

lights_headers = {
    "fytopanel": [
        "coolwhite",  # "channel-1",  # white
        "blue",       # "channel-2",  # blue
        "green",      # "channel-3",  # green
        "red",        # "channel-4",  # red
        "deepred",    # "channel-5",  #  deepred
        "farred",     # "channel-6",  # far red
        "infrared",   # "channel-7",  # infrared
        "cyan",       # "channel-8"   # NA
    ],
    "capsule": [
        "coolwhite",
        "deepred",
        "farred",
    ],
    "capsule-ext": [
        "coolwhite",
        "deepred",
        "farred",
        "royalblue",
        "blue",
        "cyan"
    ],
    "old_fytopanel": [
        "channel-1",  # white
        "channel-2",  # blue
        "channel-3",  # green
        "channel-4",  # nearest red
        "channel-5",  # near red
        "channel-6",  # far red
        "channel-7",  # infra red
        "channel-8"   # unknown, further infra red?
    ],
    "heliospectra_s7": [
        "400nm",  # channel-1",  # 400nm
        "420nm",  # channel-2",  # 420nm
        "450nm",  # channel-3",  # 450nm
        "530nm",  # channel-4",  # 530nm
        "630nm",  # channel-5",  # 630nm
        "660nm",  # channel-6",  # 660nm
        "735nm",  # channel-7"   # 735nm
    ],
    "heliospectra_s10": [
        "370nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "850nm",  # channel-9",  # 850nm
        "6500k",  # channel-10"  # 6500k
    ],
    "heliospectra_dyna": [
        "380nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "5700k",  # channel-10"  # 6500k
    ],
    "conviron": [
        "light1",
        "light2"
    ]
}


def is_night(dt, day_start=7, day_end=17):
    return not (day_start <= dt.hour < day_end)


def get_excel():
    wb = Workbook(write_only=True)
    ws = wb.create_sheet(title="timepoints")
    # datetime has 19 characters
    ws.column_dimensions['A'].width = 19
    ws.column_dimensions['B'].width = 19
    return wb, ws


def add_light_headers(ws, lights):
    ws.append(["datetime", "datetime-sim", "humidity", "temperature"] + lights_headers[lights])


def generate(settings: dict, overwrite=False):
    wb, ws = get_excel()
    fname = ""
    lights = settings.get("lights", "conviron")
    add_light_headers(ws, lights)
    start = datetime.datetime.now()
    start = start.replace(microsecond=0, second=0, minute=0, hour=0)

    length_days = settings.get('length_days', 2)
    end = start + datetime.timedelta(days=length_days)
    interval_m = settings.get("interval_m", 10)
    interval = datetime.timedelta(minutes=interval_m)
    day_start = settings.get("day_start", 7)
    day_end = settings.get("day_end", 23)

    fname += f"../files/conditions2/{interval_m}m-for-{length_days}d-{day_start:02d}00to{day_end:02d}00_"

    day_temp = settings.get("day_temp", 28)
    night_temp = settings.get("night_temp", 20)
    fname += f"{day_temp}C-{night_temp}C_"

    humidity = settings.get("humidity", 55)
    fname += f"{humidity}rh_"

    fname += f"{lights}"

    channels_scaling = settings.get("channels_scaling", [])
    if len(channels_scaling) > 0:
        if len(channels_scaling) == len(lights_headers[lights]):
            fname += f"-{channels_scaling}".replace(" ", "")

    while start < end:
        temp = day_temp
        if is_night(start, day_start=day_start, day_end=day_end):
            temp = night_temp
        channels = [100] * len(lights_headers[lights])

        if len(channels_scaling) > 0:
            if len(channels_scaling) == len(channels):
                channels = [ch * sf for ch, sf in zip(channels, channels_scaling)]
            else:
                print(f"len(channels_scaling) == {len(channels_scaling)} != len(channels) =={len(channels)}")

        if is_night(start, day_start=day_start, day_end=day_end):
            channels = [0] * len(lights_headers[lights])

        ws.append([
            start,
            start,
            humidity,
            temp
        ] + channels)
        start += interval

    if settings.get("filename"):
        print(f'saving {settings.get("filename")}')
        wb.save(filename=settings.get("filename"))
        return
    fn = f"{fname}.xlsx".replace(" ", "")

    if not os.path.isfile(fn) or overwrite:
        print(f'saved => {os.path.split(fn)[-1]}')
        wb.save(filename=fn)
    else:
        print(f'exists => {os.path.split(fn)[-1]}')
        wb.save("tmp.tmp")
        os.remove("tmp.tmp")
        # del wb
        # wb.close()


settings = {
    "interval_m": 10,
    "day_temp": 28,
    "night_temp": 20,
    "day_start": 7,
    "day_end": 23,
    "humidity": 55
}

settings['lights'] = "heliospectra_s10"

generate(settings)

settings['lights'] = "heliospectra_s7"

generate(settings)

settings['lights'] = "fytopanel"

generate(settings)


settings = {
    "interval_m": 10,
    "day_temp": 28,
    "night_temp": 20,
    "day_start": 7,
    "day_end": 17,
    "humidity": 55
}

settings['lights'] = "heliospectra_s10"

generate(settings)

settings['lights'] = "heliospectra_s7"

generate(settings)

settings['lights'] = "fytopanel"

generate(settings)


settings = {
    "interval_m": 60,
    "day_temp": 21,
    "night_temp": 21,
    "day_start": 9,
    "day_end": 21,
    "humidity": 55
}

settings['lights'] = "heliospectra_s10"

generate(settings)

settings['lights'] = "heliospectra_s7"

generate(settings)

settings['lights'] = "fytopanel"

generate(settings)


settings = {
    "interval_m": 10,
    "day_temp": 21,
    "night_temp": 21,
    "day_start": 9,
    "day_end": 21,
    "humidity": 55
}

settings['lights'] = "heliospectra_s10"

generate(settings)

settings['lights'] = "heliospectra_s7"

generate(settings)

# settings for ch36 2020-02-24
settings = {
    "lights": "fytopanel",
    "interval_m": 10,
    "day_temp": 28,
    "night_temp": 22,
    "day_start": 6,
    "day_end": 22,
    "humidity": 60,
    "channels_scaling": [
        1.0,  # white
        1.0,  # blue
        1.0,  # green
        0.6,  # nearest red
        0.6,  # near red
        0.6,  # far red
        0.0,  # infra red
        0.0,  # unknown, further infra red?
    ]
}

generate(settings)


# settings for gc05 2020-07-20
# 10m-for-2d-0800to2400_28C-28C_60rh_heliospectra_s7-[0.5,0.5,0.5,0.5,0.5,0.5,0.5].xlsx
settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 28,
    "night_temp": 28,
    "day_start": 8,
    "day_end": 24,
    "humidity": 60,
    "channels_scaling": [
        0.5,  # channel-1",  # 400nm
        0.5,  # channel-2",  # 420nm
        0.5,  # channel-3",  # 450nm
        0.5,  # channel-4",  # 530nm
        0.5,  # channel-5",  # 630nm
        0.5,  # channel-6",  # 660nm
        0.5,  # channel-7"   # 735nm
    ]
}

generate(settings)

# settings for gc03 and 4 2020-07-22
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0.5,  # channel-1",  # 370nm
        0.5,  # channel-2",  # 400nm
        0.5,  # channel-3",  # 420nm
        0.5,  # channel-4",  # 450nm
        0.5,  # channel-5",  # 530nm
        0.5,  # channel-6",  # 620nm
        0.5,  # channel-7",  # 660nm
        0.5,  # channel-8",  # 735nm
        0.5,  # channel-10"  # 6500k
    ],
}

generate(settings)

# settings for gc03 and 4 2020-08-10
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 24,
    "humidity": 60,
}

generate(settings)

# test for gc14 2020-08-18
settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0.1,  # channel-1",  # 400nm
        0.1,  # channel-2",  # 420nm
        0.1,  # channel-3",  # 450nm
        0.1,  # channel-4",  # 530nm
        0.1,  # channel-5",  # 630nm
        0.1,  # channel-6",  # 660nm
        0.1,  # channel-7"   # 735nm
    ],
}

generate(settings)

# test for gc05 2020-09-14
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 16,
    "humidity": 60
}
generate(settings)


# test for gc14 2020-09-14
settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0,  # channel-4",  # 530nm
        0,  # channel-5",  # 630nm
        0.1,  # channel-6",  # 660nm
        0.1,  # channel-7"   # 735nm
    ],
}
generate(settings)

# test for gc03 2020-10-08
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 20,
    "day_start": 13,
    "day_end": 16,
    "humidity": 60
}
generate(settings)

# settings for gc04 test deployment 2020-10-12
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 30,
    "night_temp": 25,
    "day_start": 10,
    "day_end": 16,
    "humidity": 60,
    "channels_scaling": [
        0.5,  # channel-1",  # 370nm
        0.5,  # channel-2",  # 400nm
        0.5,  # channel-3",  # 420nm
        0.5,  # channel-4",  # 450nm
        0.5,  # channel-5",  # 530nm
        0.5,  # channel-6",  # 620nm
        0.5,  # channel-7",  # 660nm
        0.5,  # channel-8",  # 735nm
        0.5,  # channel-10"  # 6500k
    ],
}
generate(settings)

# test for gc03 2020-10-15
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 20,
    "day_start": 11,
    "day_end": 16,
    "humidity": 60
}
generate(settings)

# new gc14 settings 2020-10-29
settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0,  # channel-4",  # 530nm
        0.05,  # channel-5",  # 630nm
        0.1,  # channel-6",  # 660nm
        0.3,  # channel-7"   # 735nm
    ],
}
generate(settings)

# new gc14 settings 2020-11-10

settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0.11,  # channel-4",  # 530nm
        0.11,  # channel-5",  # 630nm
        0.07,  # channel-6",  # 660nm
        0.5,  # channel-7"   # 735nm
    ],
}
generate(settings)

# test for gc04 2020-11-20
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 7,
    "day_end": 23,
    "humidity": 60
}
generate(settings)

# new soilcquest settings 2020-12-16
settings = {
    "lights": "heliospectra_s10",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 7,
    "day_end": 24,
    "humidity": 60,
}
generate(settings)

# ooops. new soilcquest settings 2020-12-17
settings = {
    "lights": "heliospectra_s10",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 17,
    "day_start": 7,
    "day_end": 23,
    "humidity": 50,
}
generate(settings)

# new gc14 settings 2021-01-28

settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0,  # channel-4",  # 530nm
        0,  # channel-5",  # 630nm
        0.03,  # channel-6",  # 660nm
        0.7,  # channel-7"   # 735nm
    ],
}
generate(settings)

# new gc14 settings 2021-02-02

settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0,  # channel-4",  # 530nm
        0,  # channel-5",  # 630nm
        0.2,  # channel-6",  # 660nm
        0.7,  # channel-7"   # 735nm
    ],
}
generate(settings)

# GC05 settings for frederike 2021-02-17
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 6,
    "day_end": 22,
    "humidity": 60,
    "channels_scaling": [
        0.6,
        0.6,
        0.6,
        0.6,
        0.6,
        0.6,
        0.6,
        0.6,
        0.6
    ]
}

generate(settings)

# GC36 settings for Penny 2021-03-16
settings = {
    "lights": "fytopanel",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 7,
    "day_end": 22,
    "humidity": 68,
    "channels_scaling": [
        1.0,  # white
        1.0,  # blue
        1.0,  # green
        1.0,  # 0.6,  # nearest red
        1.0,  # 0.6,  # near red
        1.0,  # 0.6,  # far red
        1.0,  # 0.0,  # infra red
        1.0,  # 0.0,  # unknown, further infra red?
    ]
}

generate(settings)

# GC03 settings for Penny (Maria Ermakova) 2021-05-19
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 1,
    "day_end": 23,
    "humidity": 60
}

generate(settings)

# GC36 settings for Penny (Nicole Pontarin) 2021-06-09
settings = {
    "lights": "fytopanel",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 6,
    "day_end": 20,
    "humidity": 68,
    "channels_scaling": [
        1.0,  # white
        1.0,  # blue
        1.0,  # green
        1.0,  # 0.6,  # nearest red
        1.0,  # 0.6,  # near red
        1.0,  # 0.6,  # far red
        1.0,  # 0.0,  # infra red
        1.0,  # 0.0,  # unknown, further infra red?
    ]
}

generate(settings)

# new wolfram GC35 intermediate settings 2021-06-11
settings = {
    "lights": "heliospectra_s10",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 17,
    "day_start": 6,
    "day_end": 18,
    "humidity": 60,
}

generate(settings)

# new gc14 settings 2021-10-06

settings = {
    "lights": "heliospectra_s7",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 0,
    "day_end": 20,
    "humidity": 60,
    "channels_scaling": [
        0,  # channel-1",  # 400nm
        0,  # channel-2",  # 420nm
        0,  # channel-3",  # 450nm
        0,  # channel-4",  # 530nm
        0,  # channel-5",  # 630nm
        0.2,  # channel-6",  # 660nm
        0.9,  # channel-7"   # 735nm
    ],
}
generate(settings)

# settings for ch36 diego testing
settings = {
    "lights": "fytopanel",
    "interval_m": 10,
    "day_temp": 21,
    "night_temp": 21,
    "day_start": 9,
    "day_end": 17,
    "humidity": 70,
    "channels_scaling": [
        0.2335,  # white
        0.2335,  # blue
        0.2335,  # green
        0.2335,  # nearest red
        0.2335,  # near red
        0.2335,  # far red
        0.2335,  # infra red
        0.0,  # unknown, further infra red?
    ]
}

generate(settings)

# new GC02 settings, 4/5/22
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 10,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 6,
    "day_end": 22,
    "humidity": 60
}

generate(settings)

# GC03 test for James Nix, 17/8/22
settings = {
    "lights": "heliospectra_dyna",
    "interval_m": 1,
    "day_temp": 25,
    "night_temp": 25,
    "day_start": 6,
    "day_end": 22,
    "humidity": 60
}

generate(settings)

# annamaria gc37 2020-12-16
settings = {
    "lights": "heliospectra_s10",
    "interval_m": 60,
    "day_temp": 21,
    "night_temp": 21,
    "day_start": 6,
    "day_end": 22,
    "humidity": 55,
}
generate(settings)


def clamp(n, minimum=0, maximum=100):
    n = max(n, minimum)
    n = min(n, maximum)
    return n


def create_capsule_test_settings(lights):
    wb, ws = get_excel()
    add_light_headers(ws, lights)
    start = datetime.datetime.now()
    start = start.replace(microsecond=0, second=0, minute=0, hour=0)
    current = datetime.datetime.now()
    current = current.replace(microsecond=0, second=0, minute=0, hour=0)
    interval = datetime.timedelta(minutes=1)
    section_length = datetime.timedelta(hours=12)

    n_lights = len(lights_headers[lights])

    # section for fast interval, hourly randomised
    temp = 25
    humidity = 55
    channels = [0] * 6
    while current < start + section_length:

        if (current.timestamp() // 60) % 60 == 0:
            temp = random.randint(0, 40)
            humidity = random.randint(30, 90)
            channels = [random.randint(0, 100) for _ in range(n_lights)]
        ws.append([
            current,
            current,
            humidity,
            temp
        ] + channels)
        current += interval
    start = current

    # section for sine wave conditions at 5 min interval
    section_length = datetime.timedelta(hours=12)
    interval = datetime.timedelta(minutes=10)
    temp_range = (10, 30)
    humidity_range = (40, 80)
    i = 0
    interval = datetime.timedelta(minutes=5)
    temp_range = (10, 30)
    temp_amp = temp_range[1] - temp_range[0]
    humidity_range = (40, 80)
    humidity_amp = humidity_range[1] - humidity_range[0]
    freq = 3600 * 2
    channels = [0] * n_lights
    while current < start + section_length:
        current += interval
        i = (current.timestamp() - start.timestamp()) / freq
        # scaling to get sin between 0 and 1
        sin = math.sin(i * math.pi) / 2 + 0.5
        temp = temp_range[0] + (sin * temp_amp)
        humidity = humidity_range[0] + (sin * humidity_amp)
        for i, c in enumerate(channels):
            channels[i] = round(clamp(-50 + (sin * 150)), 2)

        ws.append([
            current,
            current,
            round(humidity, 2),
            round(temp, 2)
        ] + channels)

    wb.save(f"../files/conditions2/test_conditions-{lights}.xlsx")


create_capsule_test_settings("capsule")
create_capsule_test_settings("capsule-ext")
